﻿ using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Development : MonoBehaviour {

    public static List<List<string>> commercialUnits = new List<List<string>>();
    public static List<List<string>> officeUnits = new List<List<string>>();
    private static bool first = true;

    //This is sent to blockController.cs to see in which positions growth is possibel
    public List<string> growPositions;
    public int cubeTypeNumber;

    public GameObject SelectedPanel;
    public GameObject phasePanel;
    public Text selectedText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Develop()
    {
        Debug.Log("DEVELOPMENT FUNCTION (Development.cs) ACTIVATED");
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseActive = true;
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().growCubes = false;
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().changeText("III. Development. Click to continue.");
        
        CheckCommercialCubes();
        CheckResidentialCubes();
    }

    //Called from PhaseController.development
    public void CheckResidentialCubes()
    {
       

        //Create a table for commercial units as well
        createCommercialUnitTable();
        createOfficeUnitTable();

        //Create temporary list of Residential units to check
        List<List<string>> ResidentialTempUnits = PointsController.units;
        //Create temporary list of Office units to check

        List<List<string>> OfficeTempUnits = officeUnits;

        string cubeToTest = "";
        

        //Loop through all residential units
        for (int i = 0; i < ResidentialTempUnits.Count; i++)
        {
            Debug.Log("Looping residential units, index: " + i);


            int adjacentToUnit = 0;
            //Loop through residential cubes in current residential unit
            foreach (string cube in ResidentialTempUnits[i])
            {

                Debug.Log("Looping residential cubes in unit");

                //Locations to test:
                int x = System.Convert.ToInt32(cube.Substring(cube.Length - 3, 1));
                int y = System.Convert.ToInt32(cube.Substring(cube.Length - 2, 1));
                int z = System.Convert.ToInt32(cube.Substring(cube.Length - 1, 1));

                //TODO: Add check if z = 0 -> don't put it in testLocations!

                string[] testLocations = new string[6] 
                        {
                            (x-1).ToString()+y.ToString()+z.ToString(),
				            (x+1).ToString()+y.ToString()+z.ToString(),
				            x.ToString()+(y-1).ToString()+z.ToString(),
				            x.ToString()+(y+1).ToString()+z.ToString(),
				            x.ToString()+y.ToString()+(z-1).ToString(),
				            x.ToString()+y.ToString()+(z+1).ToString(),
			            };

                

                //Test for adjacent cubes in each cube if looping residential cubes
                if (GameObject.Find(cube).GetComponent<BlockController>().blockType == "Residential")
                {
                  
                    

                    //Run a loop to test the testLocations

                    foreach (string testLocation in testLocations)
                    {
                        //If testLocation is valid, set cubeToTest to it
                        if (testLocation != null)
                        {
                            cubeToTest = "cube_" + testLocation;
                            Debug.Log("The location tested is currently: " + testLocation);
                        }
                        //CHANGE THIS IF to "when this location is not tested yet". TODO: Probably not necessary anymore since the unit # is removed from list insted (yay for simplicity!)
                        if (true)
                        {
                            

                            //Check if cubeToTest is not null and if its type is Office
                            if (GameObject.Find(cubeToTest) != null && GameObject.Find(cubeToTest).GetComponent<BlockController>().blockType == "Office")
                            {
                                Debug.Log("Found adjacent Office block!");
                                adjacentToUnit++;

                                //If the cube adjacent to this Residential cube is a Office one, remove the Office unit and don't check it again
                                for (int loopIndex = 0; loopIndex < OfficeTempUnits.Count; loopIndex++)
                                {
                                    OfficeTempUnits.RemoveAt(loopIndex);
                                    Debug.Log("Removed Office unit at " + loopIndex + " !");
                                }
                            }
                            else
                            {
                                //Run this if the object adjacent wasn't an office cube
                                Debug.Log("Adjacent cube" + cubeToTest + " not office!");
                            }

                        }
                    }

                }
                Debug.Log("adjacent blocks to current unit: " + adjacentToUnit);
                //If more of adjacent units (compared to cube count in the current unit)
                if (adjacentToUnit > ResidentialTempUnits[i].Count)
                {
                    Debug.Log("More or equal to unit! Grow here!");
                    ActualGrowing(i, "Residential");
                }

            }
            //Testlocation loop ends here

        }
        //Residential unit loop ends here

        
    }

    public static void checkThisCommercialAndAdjacentCubes(GameObject cube)
    {
        string testName = cube.GetComponent<BlockController>().name;
        int x = System.Convert.ToInt32(testName.Substring(testName.Length - 3, 1));
        int y = System.Convert.ToInt32(testName.Substring(testName.Length - 2, 1));
        int z = System.Convert.ToInt32(testName.Substring(testName.Length - 1, 1));
        string[] testLocations = new string[6] {(x-1).ToString()+y.ToString()+z.ToString(),
			(x+1).ToString()+y.ToString()+z.ToString(),
			x.ToString()+(y-1).ToString()+z.ToString(),
			x.ToString()+(y+1).ToString()+z.ToString(),
			x.ToString()+y.ToString()+(z-1).ToString(),
			x.ToString()+y.ToString()+(z+1).ToString(),
		};

        // Test each adjacent location 
        bool addToUnit = false;
        int unitNro = -1;
        foreach (string testLocation in testLocations)
        {
            string testCube = "cube_" + testLocation;
            // If adjacent location is already in a unit, add this unit's children to same unit
            //foreach (List<string> unit in units){
            for (int i = 0; i < commercialUnits.Count; i++)
            {
                foreach (string cubeInUnit in commercialUnits[i])
                {
                    if (cubeInUnit == testCube)
                    {
                        addToUnit = true;
                        unitNro = i;
                    }
                }
            }
        }
        // Add this cube and all adjacent cubes to unit if they are not in it and are Commercial
        if (addToUnit)
        {
            bool alreadyinUnit = false;

            // Add currenct cube to unit
            foreach (string cubeInUnit in commercialUnits[unitNro])
            {
                if (cube.GetComponent<BlockController>().name == cubeInUnit)
                {
                    alreadyinUnit = true;
                }
            }
            if (alreadyinUnit == false)
            {
                commercialUnits[unitNro].Add(cube.GetComponent<BlockController>().name);
            }

            // Add adjacent cubes to unit
            foreach (string testLocation in testLocations)
            {
                string testCube = "cube_" + testLocation;
                alreadyinUnit = false;
                foreach (string cubeInUnit in commercialUnits[unitNro])
                {
                    if (testCube == cubeInUnit)
                    {
                        alreadyinUnit = true;
                    }
                }
                if (alreadyinUnit == false)
                {
                    if (GameObject.Find(testCube) != null && GameObject.Find(testCube).GetComponent<BlockController>().blockType == "Commercial")
                    {
                        commercialUnits[unitNro].Add(testCube);
                    }
                }
            }
        }
        else
        {
            List<string> helpList = new List<string>();
            helpList.Add(cube.GetComponent<BlockController>().name);
            commercialUnits.Add(helpList);

        }

    }
    public static void createCommercialUnitTable()
    {
        commercialUnits = new List<List<string>>();
        foreach (GameObject cube in GameObject.FindGameObjectsWithTag("gameCube"))
        {
            if (cube.GetComponent<BlockController>().blockType != "Commercial" || cube.GetComponent<BlockController>().name == "origin_cube")
            {
                continue;
            }
            if (first)
            {
                List<string> helpList = new List<string>();
                helpList.Add(cube.GetComponent<BlockController>().name);
                commercialUnits.Add(helpList);
                first = false;
                continue;
            }
            checkThisCommercialAndAdjacentCubes(cube);

            string testName = cube.GetComponent<BlockController>().name;
            int x = System.Convert.ToInt32(testName.Substring(testName.Length - 3, 1));
            int y = System.Convert.ToInt32(testName.Substring(testName.Length - 2, 1));
            int z = System.Convert.ToInt32(testName.Substring(testName.Length - 1, 1));
            string[] testLocations = new string[6] {(x-1).ToString()+y.ToString()+z.ToString(),
				(x+1).ToString()+y.ToString()+z.ToString(),
				x.ToString()+(y-1).ToString()+z.ToString(),
				x.ToString()+(y+1).ToString()+z.ToString(),
				x.ToString()+y.ToString()+(z-1).ToString(),
				x.ToString()+y.ToString()+(z+1).ToString(),
			};
            foreach (string testLocation in testLocations)
            {
                string testCube = "cube_" + testLocation;
                if (GameObject.Find(testCube) != null && GameObject.Find(testCube).GetComponent<BlockController>().blockType == "Commercial")
                {
                    checkThisCommercialAndAdjacentCubes(GameObject.Find(testCube));
                }
            }
        }
       // printDebug();


    }

    public static void checkThisOfficeAndAdjacentCubes(GameObject cube)
    {
        string testName = cube.GetComponent<BlockController>().name;
        int x = System.Convert.ToInt32(testName.Substring(testName.Length - 3, 1));
        int y = System.Convert.ToInt32(testName.Substring(testName.Length - 2, 1));
        int z = System.Convert.ToInt32(testName.Substring(testName.Length - 1, 1));
        string[] testLocations = new string[6] {(x-1).ToString()+y.ToString()+z.ToString(),
			(x+1).ToString()+y.ToString()+z.ToString(),
			x.ToString()+(y-1).ToString()+z.ToString(),
			x.ToString()+(y+1).ToString()+z.ToString(),
			x.ToString()+y.ToString()+(z-1).ToString(),
			x.ToString()+y.ToString()+(z+1).ToString(),
		};

        // Test each adjacent location 
        bool addToUnit = false;
        int unitNro = -1;
        foreach (string testLocation in testLocations)
        {
            string testCube = "cube_" + testLocation;
            // If adjacent location is already in a unit, add this unit's children to same unit
            //foreach (List<string> unit in units){
            for (int i = 0; i < officeUnits.Count; i++)
            {
                foreach (string cubeInUnit in officeUnits[i])
                {
                    if (cubeInUnit == testCube)
                    {
                        addToUnit = true;
                        unitNro = i;
                    }
                }
            }
        }
        // Add this cube and all adjacent cubes to unit if they are not in it and are Offices
        if (addToUnit)
        {
            bool alreadyinUnit = false;

            // Add currenct cube to unit
            foreach (string cubeInUnit in officeUnits[unitNro])
            {
                if (cube.GetComponent<BlockController>().name == cubeInUnit)
                {
                    alreadyinUnit = true;
                }
            }
            if (alreadyinUnit == false)
            {
                officeUnits[unitNro].Add(cube.GetComponent<BlockController>().name);
            }

            // Add adjacent cubes to unit
            foreach (string testLocation in testLocations)
            {
                string testCube = "cube_" + testLocation;
                alreadyinUnit = false;
                foreach (string cubeInUnit in officeUnits[unitNro])
                {
                    if (testCube == cubeInUnit)
                    {
                        alreadyinUnit = true;
                    }
                }
                if (alreadyinUnit == false)
                {
                    if (GameObject.Find(testCube) != null && GameObject.Find(testCube).GetComponent<BlockController>().blockType == "Office")
                    {
                        officeUnits[unitNro].Add(testCube);
                    }
                }
            }
        }
        else
        {
            List<string> helpList = new List<string>();
            helpList.Add(cube.GetComponent<BlockController>().name);
            officeUnits.Add(helpList);

        }

    }
    public static void createOfficeUnitTable()
    {
        officeUnits = new List<List<string>>();
        foreach (GameObject cube in GameObject.FindGameObjectsWithTag("gameCube"))
        {
            if (cube.GetComponent<BlockController>().blockType != "Office" || cube.GetComponent<BlockController>().name == "origin_cube")
            {
                continue;
            }
            if (first)
            {
                List<string> helpList = new List<string>();
                helpList.Add(cube.GetComponent<BlockController>().name);
                officeUnits.Add(helpList);
                first = false;
                continue;
            }
            checkThisOfficeAndAdjacentCubes(cube);

            string testName = cube.GetComponent<BlockController>().name;
            int x = System.Convert.ToInt32(testName.Substring(testName.Length - 3, 1));
            int y = System.Convert.ToInt32(testName.Substring(testName.Length - 2, 1));
            int z = System.Convert.ToInt32(testName.Substring(testName.Length - 1, 1));
            string[] testLocations = new string[6] {(x-1).ToString()+y.ToString()+z.ToString(),
				(x+1).ToString()+y.ToString()+z.ToString(),
				x.ToString()+(y-1).ToString()+z.ToString(),
				x.ToString()+(y+1).ToString()+z.ToString(),
				x.ToString()+y.ToString()+(z-1).ToString(),
				x.ToString()+y.ToString()+(z+1).ToString(),
			};
            foreach (string testLocation in testLocations)
            {
                string testCube = "cube_" + testLocation;
                if (GameObject.Find(testCube) != null && GameObject.Find(testCube).GetComponent<BlockController>().blockType == "Office")
                {
                    checkThisOfficeAndAdjacentCubes(GameObject.Find(testCube));
                }
            }
        }
        // printDebug();


    }

    public void CheckCommercialCubes()
    {
        //Create a table for commercial units as well
        createCommercialUnitTable();

        //Create temporary list of Residential units to check
        List<List<string>> ResidentialTempUnits = PointsController.units;
        //Create temporary list of Commercial units to check
        List<List<string>> CommercialTempUnits = commercialUnits;

        string cubeToTest = "";
        

        //Loop through all commercial units
        for (int i = 0; i < CommercialTempUnits.Count; i++)
        {
            Debug.Log("Looping commercial units, index: " + i);


            int adjacentToUnit = 0;
            //Loop through residential cubes in current residential unit
            foreach (string cube in CommercialTempUnits[i])
            {

                Debug.Log("Looping commercial cubes in unit");

                //Locations to test:
                int x = System.Convert.ToInt32(cube.Substring(cube.Length - 3, 1));
                int y = System.Convert.ToInt32(cube.Substring(cube.Length - 2, 1));
                int z = System.Convert.ToInt32(cube.Substring(cube.Length - 1, 1));

                string[] testLocations = new string[6] 
                        {
                            (x-1).ToString()+y.ToString()+z.ToString(),
				            (x+1).ToString()+y.ToString()+z.ToString(),
				            x.ToString()+(y-1).ToString()+z.ToString(),
				            x.ToString()+(y+1).ToString()+z.ToString(),
				            x.ToString()+y.ToString()+(z-1).ToString(),
				            x.ToString()+y.ToString()+(z+1).ToString(),
			            };

                

                //Test for adjacent cubes in each cube if looping Commercial cubes
                if (GameObject.Find(cube).GetComponent<BlockController>().blockType == "Commercial")
                {
                    //Run a loop to test the testLocations

                    foreach (string testLocation in testLocations)
                    {
                        //If testLocation is valid, set cubeToTest to it
                        if (testLocation != null)
                        {
                            cubeToTest = "cube_" + testLocation;
                            Debug.Log("The location tested is currently: " + testLocation);
                        }
                        //CHANGE THIS IF to "when this location is not tested yet".
                        if (true)
                        {
                            

                            //Check if cubeToTest is not null and if its type is Residential
                            if (GameObject.Find(cubeToTest) != null && GameObject.Find(cubeToTest).GetComponent<BlockController>().blockType == "Residential")
                            {
                                Debug.Log("Found adjacent Residential block!");
                                
                                adjacentToUnit++;
                                //If the cube adjacent to this Commercial cube is a Residential one, remove the residential unit and don't check it again
                                for (int loopIndex = 0; loopIndex < ResidentialTempUnits.Count; loopIndex++)
                                {
                                    ResidentialTempUnits.RemoveAt(loopIndex);
                                    Debug.Log("Removed residential unit at " + loopIndex + " !");
                                }
                            }
                            else
                            {
                                //Run this if the object adjacent wasn't a Residential cube
                                Debug.Log("Adjacent cube" + cubeToTest + " not Residential!");
                            }

                        }
                    }

                }
                Debug.Log("adjacent blocks to current unit: " + adjacentToUnit);
                //If more of adjacent units (compared to cube count in the current unit)
                if (adjacentToUnit > CommercialTempUnits[i].Count)
                {

                    Debug.Log("More or equal to unit! Grow here!");
                    ActualGrowing(i, "Commercial");
                }

            }
            //Testlocation loop ends here

        }
        //Commercial unit loop ends here

        
    }

    public void ActualGrowing(int unitToGrow, string unitType)
    {
        growPositions = new List<string>();

        if (unitType == "Residential")
        {
            List<string> possibleLocations = new List<string>();

            //PointsController.createUnitTable();

            possibleLocations = PointsController.units[unitToGrow];

            string cubeToTest = "";
            foreach (string cube in possibleLocations)
            {
                //Locations to test:
                int x = System.Convert.ToInt32(cube.Substring(cube.Length - 3, 1));
                int y = System.Convert.ToInt32(cube.Substring(cube.Length - 2, 1));
                int z = System.Convert.ToInt32(cube.Substring(cube.Length - 1, 1));

                string[] testLocations = new string[5] 
                        {
                            (x-1).ToString()+y.ToString()+z.ToString(),
				            (x+1).ToString()+y.ToString()+z.ToString(),
				            x.ToString()+(y-1).ToString()+z.ToString(),
				            x.ToString()+(y+1).ToString()+z.ToString(),
                            //Note here: Z axis in code is actually Y axis in Unity. Disable growing down.
				           // x.ToString()+y.ToString()+(z-1).ToString(),
				            x.ToString()+y.ToString()+(z+1).ToString(),
			            };

                foreach (string testLocation in testLocations)
                {

                    if (testLocation != null)
                    {

                        cubeToTest = "cube_" + testLocation;
                        //TODO: Remember to do an elevator check as well!
                        if (GameObject.Find(cubeToTest) == null || cubeToTest == "")
                        {
                            Debug.Log("cubeToTest at " + cubeToTest + " is null, add position to list of possible placements for growth!");
                            
                            growPositions.Add(cubeToTest);

                        }
                        else
                        {
                            Debug.Log("cubeToTest at " + cubeToTest + " is reserved! Cannot grow here.");
                        }

                    }

                }

            }
            //here, send list of possible placements to BlockController, where OnMouseDown checks if clicked position is in the list
            //Do this with setting growCubes() for now, activating it in BlockController.
            //TODO: add logic here (or in the end of this function) to disable panels / ui correctly
            cubeTypeNumber = 0;

            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().growCubes = true;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseNumber = 4;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseActive = true;
            
            SelectedPanel.SetActive(true);
            phasePanel.SetActive(false);

            selectedText = GameObject.FindGameObjectWithTag("SelectedText").GetComponent<Text>();
            selectedText.text = "Grow Cube: ";


        }
        else if (unitType == "Commercial")
        {
            List<string> possibleLocations = new List<string>();

            //PointsController.createUnitTable();

            possibleLocations = commercialUnits[unitToGrow];

            string cubeToTest = "";
            foreach (string cube in possibleLocations)
            {
                //Locations to test:
                int x = System.Convert.ToInt32(cube.Substring(cube.Length - 3, 1));
                int y = System.Convert.ToInt32(cube.Substring(cube.Length - 2, 1));
                int z = System.Convert.ToInt32(cube.Substring(cube.Length - 1, 1));

                string[] testLocations = new string[5] 
                        {
                            (x-1).ToString()+y.ToString()+z.ToString(),
				            (x+1).ToString()+y.ToString()+z.ToString(),
				            x.ToString()+(y-1).ToString()+z.ToString(),
				            x.ToString()+(y+1).ToString()+z.ToString(),
                            //Note here: Z axis in code is actually Y axis in Unity. Disable growing down.
				           // x.ToString()+y.ToString()+(z-1).ToString(),
				            x.ToString()+y.ToString()+(z+1).ToString(),
			            };

                foreach (string testLocation in testLocations)
                {

                    if (testLocation != null)
                    {

                        cubeToTest = "cube_" + testLocation;
                        //TODO: Remember to do an elevator check as well!
                        if (GameObject.Find(cubeToTest) == null || cubeToTest == "")
                        {
                            Debug.Log("cubeToTest at " + cubeToTest + " is null, add position to list of possible placements for growth!");

                            growPositions.Add(cubeToTest);

                        }
                        else
                        {
                            Debug.Log("cubeToTest at " + cubeToTest + " is reserved! Cannot grow here.");
                        }

                    }

                }

            }
            //here, send list of possible placements to BlockController, where OnMouseDown checks if clicked position is in the list
            //Do this with setting growCubes() for now, activating it in BlockController.
            //TODO: add logic here (or in the end of this function) to disable panels / ui correctly
            cubeTypeNumber = 2;

            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().growCubes = true;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseNumber = 4;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseActive = true;

            SelectedPanel.SetActive(true);
            phasePanel.SetActive(false);

            selectedText = GameObject.FindGameObjectWithTag("SelectedText").GetComponent<Text>();
            selectedText.text = "Grow Cube: ";


        }

    }

    

}