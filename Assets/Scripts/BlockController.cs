﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BlockController : MonoBehaviour
{
    public int height;
    public int max_height;

    public string blockType;
    public int blockType_number;
    public Texture[] textures;

    public bool isSelected;

    private string[] cubeTypes = new string[6] { "Residential", "Office", "Commercial", "AC/DC", "Elevator", "City Hall" };

    public GameObject phasePanel;

    public GameObject confirmButton;

    public GameObject SelectedPanel;
    public GameObject SelectedCube;

    public int ui_cube_no;

    public GameObject hideButton;

    //Set in PhaseController - Development
    public bool shouldGrow;


    // Use this for initialization
    void Start()
    {
        phasePanel = GameObject.FindGameObjectWithTag("phasePanel");

        GameObject[] base_cubes = GameObject.FindGameObjectsWithTag("base_cube");
        GameObject[] sub_base_cubes = GameObject.FindGameObjectsWithTag("sub_base_cube");
        foreach (GameObject base_cube in base_cubes)
        {
            base_cube.GetComponent<Renderer>().enabled = false;
        }

        foreach (GameObject sub_base_cube in sub_base_cubes)
        {
            sub_base_cube.GetComponent<Renderer>().enabled = false;
        }


    }

    // Update is called once per frame
    void Update()
    {
		if (GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().growCubes)
		{
			GameObject selectedCube = GameObject.FindGameObjectWithTag("SelectedCube");
			selectedCube.GetComponent<BlockController>().blockType_number = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Development>().cubeTypeNumber;
			setType(selectedCube);
		}
    }

	public void placeTownHall()
	{
		isSelected = true;
		GameObject.Find("origin_cube").GetComponent<BlockController>().blockType_number = 5;
		GameObject.Find("origin_cube").GetComponent<BlockController>().blockType = "City Hall";
		SelectedCube.GetComponent<BlockController>().blockType_number = 5;
		SelectedCube.GetComponent<BlockController> ().blockType = "City Hall";
		setType(SelectedCube);
		//SelectedCube.GetComponent<BlockController>().ui_cube_no = 0;

		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().canPlaceBlocks = true;
		phasePanel.SetActive(false);
		SelectedPanel.SetActive(true);
		hideButton.SetActive(false);

	}

    void OnMouseDown()
    {
        if (this.tag == "SelectedCube")
        {
            return;
        }

        if (this.tag == "ui_cube")
        {


            //not debug
            isSelected = true;
            GameObject.Find("origin_cube").GetComponent<BlockController>().blockType_number = this.GetComponent<BlockController>().blockType_number;
            GameObject.Find("origin_cube").GetComponent<BlockController>().blockType = this.GetComponent<BlockController>().blockType;
            SelectedCube.GetComponent<BlockController>().blockType_number = this.gameObject.GetComponent<BlockController>().blockType_number;
            SelectedCube.GetComponent<BlockController>().ui_cube_no = this.gameObject.GetComponent<BlockController>().ui_cube_no;
            setType(SelectedCube);

            if (GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().discardBlock)
            {
                confirmButton.SetActive(true);
                SelectedPanel.SetActive(true);


            }

            else
            {
                //Quick debug, user clicks on ui_cube -> disable phases
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().canPlaceBlocks = true;
                phasePanel.SetActive(false);
                this.gameObject.SetActive(false);
                SelectedPanel.SetActive(true);
                hideButton.SetActive(false);


            }

        }
        //Else for placed blocks, if canPlaceBlocks is true in PhaseController.cs
        else if (GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().canPlaceBlocks)
        {
            // We are placing City Hall before the first round
            if (GameObject.Find("origin_cube").GetComponent<BlockController>().blockType == "City Hall")
            {
                GameObject cubeSpawn = (GameObject)Instantiate(GameObject.Find("origin_cube"), transform.position + new Vector3(0, 1, 0), transform.rotation);
                /*
                if (this.height != 0)
                {
                    cubeSpawn.transform.position = cubeSpawn.transform.position + new Vector3(0, 1, 0);
                }*/
                cubeSpawn.GetComponent<BlockController>().height = this.height + 1;
                cubeSpawn.GetComponent<BlockController>().max_height = this.max_height;
                string testName1 = "cube_" + this.name.Substring(this.name.Length - 2, 2) + "1";
                cubeSpawn.name = testName1;

                int i = GameObject.Find("origin_cube").GetComponent<BlockController>().blockType_number;

                setType(cubeSpawn);

                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().canPlaceBlocks = false;
                phasePanel.SetActive(true);
                SelectedPanel.SetActive(false);
                hideButton.SetActive(true);
                PointsController.updatePointsToScreen();


                Debug.Log("City Hall placed");
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().blocksPlaced = 0;
                //Set discardBlock to true in the beginning PhaseController - investment instead of in the end of Phase 2
                //GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().discardBlock = true;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseActive = false;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseNumber = 1;
            }

            if (this.name.Substring(0, this.name.Length - 1) == "color_button_")
            {
                string i = this.name.Substring(this.name.Length - 1, 1);
                GameObject.Find("origin_cube").GetComponent<BlockController>().blockType_number = System.Convert.ToInt32(i);
                GameObject.Find("origin_cube").GetComponent<BlockController>().blockType = cubeTypes[System.Convert.ToInt32(i)];
                return;
            }
            int h = this.height + 1;
            string testName = "";
            //Test if the cube is a suburb cube or a base cube and set testName to position in level
            if (this.name.Substring(0, this.name.Length - 2) == "base_cube_" || this.name.Substring(0, this.name.Length - 2) == "sub_base_cube_")
            {
                testName = "cube_" + this.name.Substring(this.name.Length - 2, 2) + h.ToString();
            }
            else
            {
                testName = "cube_" + this.name.Substring(this.name.Length - 3, 2) + h.ToString();
            }


            // Check if the cube is residential => can't be placed adjacent to them
            if (GameObject.Find("origin_cube").GetComponent<BlockController>().blockType == "Residential")
            {
                int x = System.Convert.ToInt32(testName.Substring(testName.Length - 3, 1));
                int y = System.Convert.ToInt32(testName.Substring(testName.Length - 2, 1));
                int z = System.Convert.ToInt32(testName.Substring(testName.Length - 1, 1));
                string[] testLocations = new string[6] {(x-1).ToString()+y.ToString()+z.ToString(),
                                                        (x+1).ToString()+y.ToString()+z.ToString(),
                                                        x.ToString()+(y-1).ToString()+z.ToString(),
                                                        x.ToString()+(y+1).ToString()+z.ToString(),
                                                        x.ToString()+y.ToString()+(z-1).ToString(),
                                                        x.ToString()+y.ToString()+(z+1).ToString(),
                };
                foreach (string testLocation in testLocations)
                {
                    string testCube = "cube_" + testLocation;
                    if (GameObject.Find(testCube) != null && GameObject.Find(testCube).GetComponent<BlockController>().blockType == "Residential")
                    {
                        Debug.Log(testCube + " is adjacent to " + testName + " and cannot be placed there!");
                        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<UIController>().showCantPlace("You can't place a Residential cube next to another Residential cube!");
                        return; // Adjacent to Residential cube => can't be placed!
                    }
                }

            }


            // Check if the cube is commercial => can't be placed adjacent to them
            if (GameObject.Find("origin_cube").GetComponent<BlockController>().blockType == "Commercial")
            {
                int x = System.Convert.ToInt32(testName.Substring(testName.Length - 3, 1));
                int y = System.Convert.ToInt32(testName.Substring(testName.Length - 2, 1));
                int z = System.Convert.ToInt32(testName.Substring(testName.Length - 1, 1));
                string[] testLocations = new string[6] {(x-1).ToString()+y.ToString()+z.ToString(),
				(x+1).ToString()+y.ToString()+z.ToString(),
				x.ToString()+(y-1).ToString()+z.ToString(),
				x.ToString()+(y+1).ToString()+z.ToString(),
				x.ToString()+y.ToString()+(z-1).ToString(),
				x.ToString()+y.ToString()+(z+1).ToString(),
			};
                foreach (string testLocation in testLocations)
                {
                    string testCube = "cube_" + testLocation;
                    if (GameObject.Find(testCube) != null && GameObject.Find(testCube).GetComponent<BlockController>().blockType == "Commercial")
                    {
                        Debug.Log(testCube + " is adjacent to " + testName + " and cannot be placed there!");
                        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<UIController>().showCantPlace("You can't place a Commercial cube next to another Commercial cube!");
                        return; // Adjacent to Commercial cube => can't be placed!
                    }
                }

            }

            // Check if the cube is elevator => can only be placed on first floor or on top of elevator
            if (GameObject.Find("origin_cube").GetComponent<BlockController>().blockType == "Elevator")
            {
                int x = System.Convert.ToInt32(testName.Substring(testName.Length - 3, 1));
                int y = System.Convert.ToInt32(testName.Substring(testName.Length - 2, 1));
                int z = System.Convert.ToInt32(testName.Substring(testName.Length - 1, 1));
                string toBeTested = "cube_" + x.ToString() + y.ToString() + (z - 1).ToString();
                Debug.Log("Cube to be tested is named " + toBeTested);
                if (GameObject.Find(toBeTested) != null && GameObject.Find(toBeTested).GetComponent<BlockController>().blockType != "Elevator")
                {
                    Debug.Log("Elevator can't be placed there");
                    GameObject.FindGameObjectWithTag("MainCamera").GetComponent<UIController>().showCantPlace("You can only place Elevator on top of Elevator or on ground floor!");
                    return;
                }
            }

            // Non-elevators can't be placed on top of elevators
            if (GameObject.Find("origin_cube").GetComponent<BlockController>().blockType != "Elevator")
            {
                int x = System.Convert.ToInt32(testName.Substring(testName.Length - 3, 1));
                int y = System.Convert.ToInt32(testName.Substring(testName.Length - 2, 1));
                int z = System.Convert.ToInt32(testName.Substring(testName.Length - 1, 1));
                string toBeTested = "cube_" + x.ToString() + y.ToString() + (z - 1).ToString();

                if (GameObject.Find(toBeTested) != null && GameObject.Find(toBeTested).GetComponent<BlockController>().blockType == "Elevator")
                {
                    Debug.Log("Can't place non-elevator over elevator");
                    GameObject.FindGameObjectWithTag("MainCamera").GetComponent<UIController>().showCantPlace("Only elevator cubes can be placed over elevators!");
                    return;
                }
            }



            if (GameObject.Find(testName) == null)
            {
                if (this.height < this.max_height)
                {
                    int elevatorHeight = 1;

                    for (int x = 0; x < 5; x++)
                    {
                        for (int y = 0; y < 5; y++)
                        {
                            string elevatorHeightTest = "cube_" + x.ToString() + y.ToString() + "1";
                            string[] testLocations = new string[4] {(x-1).ToString()+y.ToString()+"1",
								(x+1).ToString()+y.ToString()+"1",
								x.ToString()+(y-1).ToString()+"1",
								x.ToString()+(y+1).ToString()+"1",
							};
                            if (GameObject.Find(elevatorHeightTest) == null || GameObject.Find(elevatorHeightTest).GetComponent<BlockController>().blockType != "Elevator")
                            {
                                continue;
                            }
                            Debug.Log("Elevator found at " + elevatorHeightTest);
                            foreach (string testLocation in testLocations)
                            {
                                Debug.Log("Testing testLocation " + testLocation);
                                if (GameObject.Find("cube_" + testLocation) != null && GameObject.Find("cube_" + testLocation).GetComponent<BlockController>().blockType == "AC/DC")
                                {
                                    Debug.Log("testLocation " + testLocation + " is AC/DC");
                                    for (int z = 1; z <= 5; z++)
                                    {
                                        if (GameObject.Find("cube_" + x.ToString() + y.ToString() + z.ToString()) != null)
                                        {
                                            if (GameObject.Find("cube_" + x.ToString() + y.ToString() + z.ToString()).GetComponent<BlockController>().height > elevatorHeight)
                                            {
                                                elevatorHeight = GameObject.Find("cube_" + x.ToString() + y.ToString() + z.ToString()).GetComponent<BlockController>().height;
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    Debug.Log("Elevator height is " + elevatorHeight.ToString());

                    if (this.height < elevatorHeight || this.blockType == "Elevator")
                    {


                        GameObject cubeSpawn = (GameObject)Instantiate(GameObject.Find("origin_cube"), transform.position + new Vector3(0, 1, 0), transform.rotation);

                        if (this.height != 0)
                        {
                            cubeSpawn.transform.position = cubeSpawn.transform.position + new Vector3(0, 1, 0);
                        }
                        cubeSpawn.GetComponent<BlockController>().height = this.height + 1;
                        cubeSpawn.GetComponent<BlockController>().max_height = this.max_height;
                        cubeSpawn.name = testName;

                        int i = GameObject.Find("origin_cube").GetComponent<BlockController>().blockType_number;

                        setType(cubeSpawn);

                        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().canPlaceBlocks = false;
                        phasePanel.SetActive(true);
                        SelectedPanel.SetActive(false);
                        hideButton.SetActive(true);
                        PointsController.updatePointsToScreen();

                        //GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().Investment();
                        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().cubesLeft--;

                        if (GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().blocksPlaced == 0)
                        {
                            Debug.Log("First cube placed");
                            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().blocksPlaced = 1;
                        }
                        else
                        {
                            //Run development phase after placing second cube by setting phaseNumber to 3
                            Debug.Log("Second cube placed");
                            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().blocksPlaced = 0;
                            //Set discardBlock to true in the beginning PhaseController - investment instead of in the end of Phase 2
                            //GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().discardBlock = true;
                            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseActive = false;
                            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseNumber = 3;

                        }
                    }
                    else
                    {
                        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<UIController>().showCantPlace("You don't have a powered elevator that is high enough!");

                    }
                }
                else
                {
                    GameObject.FindGameObjectWithTag("MainCamera").GetComponent<UIController>().showCantPlace("Cube can't be placed this high!");
                }


            }

        }
        //Else if clicked on base cube and growing mode is enabled -> Set growing logic here
        else if (GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().growCubes && GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseNumber == 4)
        {

            Debug.Log("Entered final growing cubes!");
            Debug.Log("Clicked on: " + this.name);
            //Set growList as the list of possible locations to grow the unit into
            List<string> growList = new List<string>();
            growList = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Development>().growPositions;

            string testName = "";

            GameObject selectedCube = GameObject.FindGameObjectWithTag("SelectedCube");

            //selectedCube.GetComponent<BlockController>().blockType = "Residential";
            selectedCube.GetComponent<BlockController>().blockType_number = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Development>().cubeTypeNumber;

            setType(selectedCube);

            
            //Test if the cube is a suburb cube or a base cube and set testName to position in level
            if (this.name.Substring(0, this.name.Length - 2) == "base_cube_" || this.name.Substring(0, this.name.Length - 2) == "sub_base_cube_")
            {
                testName = "cube_" + this.name.Substring(this.name.Length - 2, 2) + (this.height+1).ToString();
            }
            else
            {
                testName = "cube_" + this.name.Substring(this.name.Length - 3, 2) + (this.height + 1).ToString();
            }
            Debug.Log("testName is " + testName);

            //Check here if clicked cube is included in the list, if so -> grow block!
            if (growList.Contains(testName))
            {
                Debug.Log("List contains " + this.name + " !");

                GameObject cubeSpawn = (GameObject)Instantiate(GameObject.Find("origin_cube"),
                            transform.position + new Vector3(0, 1, 0), transform.rotation);

                if (this.height != 0)
                {
                    cubeSpawn.transform.position = cubeSpawn.transform.position + new Vector3(0, 1, 0);

                }
                cubeSpawn.GetComponent<BlockController>().height = this.height + 1;
                cubeSpawn.GetComponent<BlockController>().max_height = this.max_height;
                cubeSpawn.name = testName;

                
                cubeSpawn.GetComponent<BlockController>().blockType_number = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Development>().cubeTypeNumber;

                setType(cubeSpawn);

                //Quick debug, set the game back to Phase 1
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().growCubes = false;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseNumber = 1;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseActive = false;
                SelectedPanel.SetActive(false);
                phasePanel.SetActive(true);

                
            }



            /*
            //Set testName as position of the cube that should be grown
            int h = this.height + 1;
            string testName = "";

            //TODO: Check sub / base cubes to fix growing up?

            foreach (GameObject cube in GameObject.FindGameObjectsWithTag("gameCube"))
            {
                if (cube.GetComponent<BlockController>().shouldGrow)
                {
                    testName = "cube_" + cube.name.Substring(cube.name.Length - 2, 2) + h.ToString();
                    Debug.Log("Cube is set as " + testName);
                    break;

                }
                else
                {
                    testName = null;
                }

            }

            //Fill testLocations string array with adjacent positions
            if (testName != null)
            {

                int x = System.Convert.ToInt32(testName.Substring(testName.Length - 3, 1));
                int y = System.Convert.ToInt32(testName.Substring(testName.Length - 2, 1));
                int z = System.Convert.ToInt32(testName.Substring(testName.Length - 1, 1));

                string[] testLocations = new string[6] 
            {   
                (x-1).ToString()+y.ToString()+z.ToString(),
				(x+1).ToString()+y.ToString()+z.ToString(),
				x.ToString()+(y-1).ToString()+z.ToString(),
				x.ToString()+(y+1).ToString()+z.ToString(),
				x.ToString()+y.ToString()+(z-1).ToString(),
				x.ToString()+y.ToString()+(z+1).ToString(),
			};


                foreach (string testLocation in testLocations)
                {
                    string testCube = "cube_" + testLocation;

                    if (GameObject.Find(testCube) != null && GameObject.Find(testCube).GetComponent<BlockController>().blockType == "Commercial")
                    {
                        Debug.Log(testCube + " is adjacent to " + testName + " and CAN be placed there!");

                        GameObject cubeSpawn = (GameObject)Instantiate(GameObject.Find("origin_cube"),
                            transform.position + new Vector3(0, 1, 0), transform.rotation);

                        if (this.height != 0)
                        {
                            cubeSpawn.transform.position = cubeSpawn.transform.position + new Vector3(0, 1, 0);

                        }
                        cubeSpawn.GetComponent<BlockController>().height = this.height + 1;
                        cubeSpawn.GetComponent<BlockController>().max_height = this.max_height;
                        cubeSpawn.name = testName;

                        cubeSpawn.GetComponent<BlockController>().blockType_number = 2;

                        setType(cubeSpawn);

                        //Quick debug, set the game back to Phase 1
                        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().growCubes = false;
                        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseNumber = 1;
                        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().phaseActive = false;
                        SelectedPanel.SetActive(false);
                        phasePanel.SetActive(true);


                    }


                }

            }

            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().growCubes = false;

        } */


        }
    }

    public void setType(GameObject block)
    {

        int i = block.GetComponent<BlockController>().blockType_number;

        switch (i)
        {
            case 0:
                block.GetComponent<BlockController>().blockType = "Residential";
                break;
            case 1:
                block.GetComponent<BlockController>().blockType = "Office";
                break;
            case 2:
                block.GetComponent<BlockController>().blockType = "Commercial";
                break;
            case 3:
                block.GetComponent<BlockController>().blockType = "AC/DC";
                break;
            case 4:
                block.GetComponent<BlockController>().blockType = "Elevator";
                break;
            case 5:
                block.GetComponent<BlockController>().blockType = "City Hall";
                break;
        }

        block.renderer.material.mainTexture = textures[block.GetComponent<BlockController>().blockType_number];

    }



}
