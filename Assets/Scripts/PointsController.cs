﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PointsController : MonoBehaviour {
	
	public static List<List<string>> units = new List<List<string>>();
	private static bool first = true;
	public static int points = 0;
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public static void checkThisAndAdjacentCubes(GameObject cube){
		string testName = cube.GetComponent<BlockController>().name;
		int x = System.Convert.ToInt32(testName.Substring(testName.Length-3,1));
		int y = System.Convert.ToInt32(testName.Substring(testName.Length-2,1));
		int z = System.Convert.ToInt32(testName.Substring(testName.Length-1,1));
		string[] testLocations = new string[6] {(x-1).ToString()+y.ToString()+z.ToString(),
			(x+1).ToString()+y.ToString()+z.ToString(),
			x.ToString()+(y-1).ToString()+z.ToString(),
			x.ToString()+(y+1).ToString()+z.ToString(),
			x.ToString()+y.ToString()+(z-1).ToString(),
			x.ToString()+y.ToString()+(z+1).ToString(),
		};
		
		// Test each adjacent location 
		bool addToUnit = false;
		int unitNro = -1;
		foreach (string testLocation in testLocations) {
			string testCube = "cube_"+testLocation;
			// If adjacent location is already in a unit, add this unit's children to same unit
			//foreach (List<string> unit in units){
			for (int i = 0; i < units.Count; i++){
				foreach (string cubeInUnit in units[i]){
					if (cubeInUnit == testCube){
						addToUnit = true;
						unitNro = i;
					}
				}
			}
		}
		// Add this cube and all adjacent cubes to unit if they are not in it and are Residential
		if (addToUnit) {
			bool alreadyinUnit = false;
			
			// Add currenct cube to unit
			foreach (string cubeInUnit in units[unitNro]){
				if (cube.GetComponent<BlockController>().name == cubeInUnit){
					alreadyinUnit = true;
				}
			}
			if (alreadyinUnit == false) {
				units[unitNro].Add (cube.GetComponent<BlockController>().name);
			}
			
			// Add adjacent cubes to unit
			foreach (string testLocation in testLocations) {
				string testCube = "cube_"+testLocation;
				alreadyinUnit = false;
				foreach (string cubeInUnit in units[unitNro]){
					if (testCube == cubeInUnit){
						alreadyinUnit = true;
					}
				}
				if (alreadyinUnit == false) {
					if (GameObject.Find (testCube) != null && GameObject.Find (testCube).GetComponent<BlockController>().blockType == "Residential"){
						units[unitNro].Add (testCube);
					}
				}
			}
		}
		else {
			List<string> helpList = new List<string>();
			helpList.Add(cube.GetComponent<BlockController>().name);
			units.Add(helpList);
			
		}
		
	}
	public static void createUnitTable(){
		units = new List<List<string>>();
		foreach (GameObject cube in GameObject.FindGameObjectsWithTag("gameCube")){ 
			if (cube.GetComponent<BlockController>().blockType != "Residential" || cube.GetComponent<BlockController>().name == "origin_cube"){
				continue;
			}
			if (first){
				List<string> helpList = new List<string>();
				helpList.Add(cube.GetComponent<BlockController>().name);
				units.Add(helpList);
				first = false;
				continue;
			}
			checkThisAndAdjacentCubes(cube);
			
			string testName = cube.GetComponent<BlockController>().name;
			int x = System.Convert.ToInt32(testName.Substring(testName.Length-3,1));
			int y = System.Convert.ToInt32(testName.Substring(testName.Length-2,1));
			int z = System.Convert.ToInt32(testName.Substring(testName.Length-1,1));
			string[] testLocations = new string[6] {(x-1).ToString()+y.ToString()+z.ToString(),
				(x+1).ToString()+y.ToString()+z.ToString(),
				x.ToString()+(y-1).ToString()+z.ToString(),
				x.ToString()+(y+1).ToString()+z.ToString(),
				x.ToString()+y.ToString()+(z-1).ToString(),
				x.ToString()+y.ToString()+(z+1).ToString(),
			};
			foreach (string testLocation in testLocations) {
				string testCube = "cube_"+testLocation;
				if (GameObject.Find(testCube) != null && GameObject.Find(testCube).GetComponent<BlockController>().blockType == "Residential"){
					checkThisAndAdjacentCubes(GameObject.Find(testCube));
				}
			}
		}
		printDebug ();
		
		
	}
	
	static void printDebug(){
		Debug.Log ("printDebug");
		foreach(List<string> unit in units){
			Debug.Log ("New unit starts here");
			foreach (string cube in unit){
				Debug.Log (cube);
			}
			Debug.Log ("Unit ends");
		}
	}
	
	private static bool isPowered (List<string> unit){
		// Implement here to check if the unit is powered
		foreach (string cubeName in unit){
			
			int x = System.Convert.ToInt32(cubeName.Substring(cubeName.Length-3,1));
			int y = System.Convert.ToInt32(cubeName.Substring(cubeName.Length-2,1));
			int z = System.Convert.ToInt32(cubeName.Substring(cubeName.Length-1,1));
			string[] testLocations = new string[6] {(x-1).ToString()+y.ToString()+z.ToString(),
				(x+1).ToString()+y.ToString()+z.ToString(),
				x.ToString()+(y-1).ToString()+z.ToString(),
				x.ToString()+(y+1).ToString()+z.ToString(),
				x.ToString()+y.ToString()+(z-1).ToString(),
				x.ToString()+y.ToString()+(z+1).ToString(),
			};
			foreach (string testLocation in testLocations) {
				string testCube = "cube_"+testLocation;
				if (GameObject.Find(testCube) != null && GameObject.Find(testCube).GetComponent<BlockController>().blockType == "AC/DC"){
					return true;
				}
			}
		}
		return false;
	}
	
	public static int calculatePoints(){
		createUnitTable ();
		points = 0;
		foreach(List<string> unit in units){
			if (isPowered(unit)) {
				int height = 0;
				// Run for from 1 to up for easier calculation
				for (int i = 1; i <= unit.Count;i++){
					points = points + i;
					if (GameObject.Find (unit[i-1]).GetComponent<BlockController>().height > height) {
						height = GameObject.Find (unit[i-1]).GetComponent<BlockController>().height;
					}
				}
				
				for (int i = 2; i <= height; i++){
					points = points + i - 1;
				}
				
			}
		}
		
		foreach (GameObject sub_cube in GameObject.FindGameObjectsWithTag("sub_base_cube")){
			int x = System.Convert.ToInt32(sub_cube.name.Substring(sub_cube.name.Length-2,1));
			int y = System.Convert.ToInt32(sub_cube.name.Substring(sub_cube.name.Length-1,1));
			int z = 1;
			if (GameObject.Find ("cube_"+x.ToString()+y.ToString()+z.ToString()) != null){
				points = points - 1;
			}
			
		}
		return points;
		
	}
	
	public static void updatePointsToScreen(){
		int p = calculatePoints ();
		GameObject.Find ("PointsText").GetComponent<Text> ().text = p.ToString ();
	}
	
	/*
	public static void createUnitTable(){
		bool first = true;
		foreach (GameObject cube in GameObject.FindGameObjectsWithTag("gameCube")){ 
			if (cube.GetComponent<BlockController>().blockType != "Residential" || cube.GetComponent<BlockController>().name == "origin_cube"){
				continue;
			}
			if (first){
				Debug.Log("units == null");
				List<string> helpList = new List<string>();
				helpList.Add(cube.GetComponent<BlockController>().name);
				units.Add(helpList);
				first = false;
				continue;
			}
			string testName = cube.GetComponent<BlockController>().name;
			Debug.Log (testName);
			int x = System.Convert.ToInt32(testName.Substring(testName.Length-3,1));
			int y = System.Convert.ToInt32(testName.Substring(testName.Length-2,1));
			int z = System.Convert.ToInt32(testName.Substring(testName.Length-1,1));
			string[] testLocations = new string[6] {(x-1).ToString()+y.ToString()+z.ToString(),
				(x+1).ToString()+y.ToString()+z.ToString(),
				x.ToString()+(y-1).ToString()+z.ToString(),
				x.ToString()+(y+1).ToString()+z.ToString(),
				x.ToString()+y.ToString()+(z-1).ToString(),
				x.ToString()+y.ToString()+(z+1).ToString(),
			};

			// Test each adjacent location 
			bool addToUnit = false;
			int unitNro = -1;
			foreach (string testLocation in testLocations) {
				string testCube = "cube_"+testLocation;
				// If adjacent location is already in a unit, add this unit's children to same unit
				//foreach (List<string> unit in units){
				for (int i = 0; i < units.Count; i++){
					foreach (string cubeInUnit in units[i]){
						if (cubeInUnit == testCube){
							addToUnit = true;
							unitNro = i;
						}
					}
				}
			}
			Debug.Log ("Line 61");
			// Add this cube and all adjacent cubes to unit if they are not in it and are Residential
			if (addToUnit) {
				bool alreadyinUnit = false;

				// Add currenct cube to unit
				foreach (string cubeInUnit in units[unitNro]){
					if (cube.GetComponent<BlockController>().name == cubeInUnit){
						alreadyinUnit = true;
					}
				}
				if (alreadyinUnit == false) {
					units[unitNro].Add (cube.GetComponent<BlockController>().name);
				}

				// Add adjacent cubes to unit
				foreach (string testLocation in testLocations) {
					string testCube = "cube_"+testLocation;
					alreadyinUnit = false;
					foreach (string cubeInUnit in units[unitNro]){
						if (testCube == cubeInUnit){
							alreadyinUnit = true;
						}
					}
					if (alreadyinUnit == false) {
						if (GameObject.Find (testCube) != null && GameObject.Find (testCube).GetComponent<BlockController>().blockType == "Residential"){
							units[unitNro].Add (testCube);
						}
					}
				}
			}
			else {
				List<string> helpList = new List<string>();
				helpList.Add(cube.GetComponent<BlockController>().name);
				units.Add(helpList);

			}

		}
		printDebug ();
	}
	*/
	
}


