﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	private GameObject creditsPanel;
	private GameObject buttons;
	private GameObject phasePanel;
	public Text hideButtonText;

	private GameObject cantPlacePanel;
	public Text cantPlaceText;

	// Use this for initialization
	void Start () {
		phasePanel = GameObject.FindGameObjectWithTag ("phasePanel");
		cantPlacePanel = GameObject.FindGameObjectWithTag ("CantPlaceError");
		cantPlacePanel.SetActive (false);
		creditsPanel = GameObject.Find ("CreditsPanel");
		creditsPanel.SetActive (false);
		buttons = GameObject.Find ("Buttons");
	}

	void Update(){

	}

	public void ZoomIn(){
		GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().distance -= 5;
		if (GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().distance < GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().distanceMin){
			GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().distance = GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().distanceMin;
		}
		GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().Rotate ();
	}

	public void ZoomOut(){
		GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().distance += 5;
		if (GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().distance > GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().distanceMax){
			GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().distance = GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().distanceMax;
		}
		GameObject.Find ("Main Camera").GetComponent<MouseOrbitImproved> ().Rotate ();
	}
	public void quit() {
		Debug.Log ("Quitting");
		Application.Quit ();
	}

	public void Hide(){
		//PointsController.createUnitTable ();
		PointsController.calculatePoints ();
		if (phasePanel.activeSelf == true){
			hideButtonText.text = "Show Selection";
			phasePanel.SetActive (false);
		}
		else if (phasePanel.activeSelf == false){
			hideButtonText.text = "Show City";
			phasePanel.SetActive (true);
		}
	}

	public void showCantPlace(string errorMsg){
		cantPlaceText.text = errorMsg;
		cantPlacePanel.SetActive (true);
		Invoke ("clearError", 3);
	}

	void clearError(){
		cantPlacePanel.SetActive (false);
	}

	public void hideMainMenu()
	{
		GameObject.Find ("MainMenu").SetActive (false);;
	}

	public void showCredits()
	{
		creditsPanel.SetActive (true);
		buttons.SetActive (false);
	}

	public void backFromCredits()
	{
		creditsPanel.SetActive (false);
		buttons.SetActive (true);
	}

	public void playAgain()
	{
		Application.LoadLevel (0);
	}


}
