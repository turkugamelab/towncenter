﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PhaseController : MonoBehaviour
{
	public GameObject quitButton;
	public GameObject playAgainButton;

    public int phaseNumber;

    public Text phaseText;
    private GameObject[] uiCubes;
    public Button phaseButton;

    public bool phaseActive;
    public bool canPlaceBlocks;

    public bool discardBlock = true;

    public int blocksPlaced;

    public int[] blockTypes_left = { 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4 };

    public int cubesLeft;

    public GameObject selectedCube;
    public bool growCubes;

    public GameObject selectedPanel;

    public Development developmentScript;

    public Text selectedText;


    // Use this for initialization
    void Start()
    {

        //selectedCube = GameObject.FindGameObjectWithTag ("SelectedCube");
        cubesLeft = 20;
        blocksPlaced = 0;
        phaseActive = false;
        canPlaceBlocks = false;

        uiCubes = GameObject.FindGameObjectsWithTag("ui_cube");

        foreach (GameObject uiCube in uiCubes)
        {
            uiCube.SetActive(false);
        }

    }

    // Update is called once per frame
    void Update()
    {
        //Set phaseNumber to 1 in update to remove Development phase
        //phaseNumber is set to 3 in BlockController after placing the second cube
        //phaseNumber = 1;


        if (phaseNumber == 1 && !phaseActive)
        {
            Investment();

        }
        if (phaseNumber == 3 && !phaseActive)
        {
            Development();
        }
        //Dirty workaround to change selectedText back, cause some errors every once in a while
		/*
        if (phaseNumber != 4 && GameObject.FindGameObjectWithTag("SelectedText") != null)
        {
            selectedText = GameObject.FindGameObjectWithTag("SelectedText").GetComponent<Text>();
            selectedText.text = "You have selected:";
        }
        */

    }
    public void NextPhase()
    {
        phaseNumber++;
        phaseButton.gameObject.SetActive(false);
		GameObject.Find ("origin_cube").GetComponent<BlockController> ().placeTownHall ();
    }

    public void Confirm()
    {


        if (phaseNumber == 1)
        {
            Debug.Log("Confirmed");
            selectedPanel.SetActive(false);


            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().returnToPool(selectedCube.GetComponent<BlockController>().blockType_number);
            //this.gameObject.SetActive(false);
            foreach (GameObject uiCube in uiCubes)
            {
                if (uiCube.GetComponent<BlockController>().ui_cube_no == selectedCube.GetComponent<BlockController>().ui_cube_no)
                {
                    uiCube.SetActive(false);
                }
            }
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().discardBlock = false;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PhaseController>().changeText("II. Construction: Select a cube to build.");
            //selectedText = GameObject.FindGameObjectWithTag("SelectedText").GetComponent<Text>();
            selectedText.text = "You have selected:";
        }
        //Else if clicked OK on Development phase, return to Phase 1 unless growing cubes
        else if (phaseNumber == 3)
        {
            Debug.Log("Clicked OK on Development");

            //Set to false to bypass growing blocks
            //growCubes = false;

            if (!growCubes)
            {
                phaseNumber = 1;
                phaseActive = false;
            }
            //Else enter grow mode
            else
            {
                GameObject.FindGameObjectWithTag("phasePanel").SetActive(false);
                GameObject selectedPanel = GameObject.FindGameObjectWithTag("gameCube").GetComponent<BlockController>().SelectedPanel;
                selectedPanel.SetActive(true);
                //Because PhaseNumber is 3 and growCubes is true, in update growblocks - function is enabled
                GameObject.FindGameObjectWithTag("SelectedText").GetComponent<Text>().text = "Choose where to grow.";
            }

        }

    }

    public void Investment()
    {
        phaseActive = true;

        foreach (GameObject uiCube in uiCubes)
        {
            uiCube.SetActive(true);
        }

        phaseText.text = "I. Investment: Select a cube to discard.";
        discardBlock = true;

        PhaseUiCubes();



    }

    public void changeText(string t)
    {
        phaseText.text = t;
    }


    public void PhaseUiCubes()
    {

        GameObject[] uiCubes = GameObject.FindGameObjectsWithTag("ui_cube");


        if (cubesLeft >= 3)
        {
            foreach (GameObject uiCube in uiCubes)
            {
                //TODO: Make an array of blocks in bag instead of random number
                //uiCube.GetComponent<BlockController>().blockType_number = Random.Range (0,5);

                int rand = Random.Range(0, 19);

                while (true)
                {
                    if (blockTypes_left[rand] != -1)
                    {
                        uiCube.GetComponent<BlockController>().blockType_number = blockTypes_left[rand];
                        blockTypes_left[rand] = -1;
                        break;
                    }
                    rand = Random.Range(0, 19);
                }

                uiCube.GetComponent<BlockController>().setType(uiCube);

            }
        }
        else if (cubesLeft == 2)
        {
            for (int i = 0; i < blockTypes_left.Length; i++)
            {
                if (blockTypes_left[i] != -1)
                {
                    uiCubes[0].GetComponent<BlockController>().blockType_number = blockTypes_left[i];
                    break;
                }
            }
            for (int i = 0; i < blockTypes_left.Length; i++)
            {
                if (blockTypes_left[i] != -1)
                {
                    uiCubes[0].GetComponent<BlockController>().blockType_number = blockTypes_left[i];
                    break;
                }
            }
            uiCubes[0].GetComponent<BlockController>().setType(uiCubes[0]);
            uiCubes[1].GetComponent<BlockController>().setType(uiCubes[1]);
            uiCubes[2].SetActive(false);
			changeText("II. Construction: Select a cube to build.");
            //selectedText = GameObject.FindGameObjectWithTag("SelectedText").GetComponent<Text>();
            selectedText.text = "You have selected:";
            discardBlock = false;
        }
        else if (cubesLeft == 0)
        {
            changeText("Game Over.\nYou scored "+PointsController.calculatePoints().ToString()+" points.");
			GameObject.Find ("ConfirmButton").SetActive(false);
			playAgainButton.SetActive(true);
			quitButton.SetActive(true);
            uiCubes[0].SetActive(false);
            uiCubes[1].SetActive(false);
            uiCubes[2].SetActive(false);
        }

    }
    public void returnToPool(int k)
    {
        for (int i = 0; i < blockTypes_left.Length; i++)
        {
            if (blockTypes_left[i] == -1)
            {
                blockTypes_left[i] = k;
                break;
            }
        }
    }



    public void Development()
    {
        developmentScript.Develop();
    }


}

















