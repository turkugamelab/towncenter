﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	private float mouseX;

	private Vector3 cameraPos;
	private Transform board;

	public float rotateSpeed = 4;
	private float rotateDir;

	void Start()
	{
		board = GameObject.FindGameObjectWithTag ("Board").transform;
		transform.LookAt (board);
	}

	void Update()
	{

		if(!Input.GetMouseButton(1))
		   return;

		mouseX = Input.mousePosition.x;

		if (mouseX < Screen.width/2) 
		{
			rotateDir = -1;
		}
		if (mouseX > Screen.width/2) 
		{
			rotateDir = 1;
		}


		transform.LookAt (board);

		cameraPos = new Vector3 
			(rotateDir * rotateSpeed, 0, 0);

		transform.Translate (cameraPos * Time.deltaTime);

		


	}



}
